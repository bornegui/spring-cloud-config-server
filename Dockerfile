FROM openjdk:13-alpine
COPY docker/id_rsa /root/.ssh/
COPY docker/config /root/.ssh/
RUN apk update; apk --no-cache add curl; apk add openssh; chmod 600 /root/.ssh/id_rsa
LABEL maintainer="Maintainer"
VOLUME /tmp
EXPOSE 8080
ARG JAR_FILE=build/libs/config-server.jar
ADD ${JAR_FILE} config-server.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/config-server.jar"]
